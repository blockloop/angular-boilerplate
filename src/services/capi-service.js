angular
    .module("app")
    .service("capiService", capiService);

function capiService($http, $q) {
    return {
        search: search,
        login: login,
        logout: logout
    };

    /*
     * Implementations
     */

    /**
     * Search for a customer
     * @param  {Object} criteria  Search criteria
     * @return {HttpPromise}
     */
    function search (criteria) {
        return $http.get("/capi/search/", {
            params: criteria
        });
    }

    /**
     * Login
     * @param  {String} username
     * @param  {String} password
     * @return {HttpPromise}
     */
    function login (username, password) {
        var deferred = $q.defer();

        var request = {
            username: username,
            password: password
        };

        $http.post("/capi/auth/login", request)
             .then(onSuccess, onError);

        return deferred.promise;

        /*
         * Callbacks
         */
        function onSuccess (response) {
            if (response == null || response.status !== 200) {
                return deferred.reject(arguments);
            }
            $http.defaults.headers.common.Authorization = "Token " + response.data.token;
            deferred.resolve();
        }

        function onError() {
            deferred.reject.call(arguments);
        }
    }

    /**
     * Login
     * @return {HttpPromise}
     */
    function logout () {
        var deferred = $q.defer();

        $http.post("/capi/auth/logout")
            .then(onSuccess, onError);

        return deferred.promise;

        function onSuccess () {
            delete $http.defaults.headers.common.Authorization;
            deferred.resolve();
        }

        function onError() {
            deferred.reject.call(arguments);
        }
    }
}


angular
    .module("app")
    .directive("pagination", Pagination);

function Pagination () {
    return {
        restrict: "E",
        templateUrl: "/partials/pagination.html",
        link: function ($scope, $element, $attrs) {
        }
    };
}


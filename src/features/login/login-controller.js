angular
    .module("app")
    .controller("LoginController", LoginController);

function LoginController ($state, capiService, $mdToast) {
    var vm = this;

    vm.login = function () {
        capiService
            .login(vm.username, vm.password)
            .then(onSuccess, onError);

        function onSuccess (data, status) {
            $state.go("home");
        }

        function onError (error) {
            var msg = null;
            if (error && error.data && error.data.message) {
                msg = error.data.message;
            }
            $mdToast.show(
                $mdToast.simple()
                    .content("ERROR! " + msg)
                    .position("top right")
                    .hideDelay(3000)
            );
        }
    };
}


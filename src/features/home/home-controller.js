angular
    .module('app')
    .controller('HomeController', HomeController);

function HomeController ($state) {
    var vm = this;

    vm.takeCall = function () {
        $state.go('search');
    };
}


angular
    .module("app")
    .controller("SearchController", SearchController);

function SearchController (capiService, $mdToast) {
    var vm = this;
    vm.pageSize = 10;

    vm.getClass = function (memberStatus) {
        return {
            "fa-check-circle green": memberStatus === "Active",
            "fa-times-circle red": memberStatus !== "Active"
        };
    };

    vm.selectResult = function (result) {
    };

    vm.doSearch = function (page) {
        if (!angular.isNumber(page)) {
            page = 1;
        }

        var request = angular.extend({
            page: page,
            pageSize: vm.pageSize
        }, vm.search);

        return capiService
            .search(request)
            .then(onSuccess, onError);

        /*
         * Callbacks
         */
        function onSuccess (response) {
            vm.currentPage = page;
            angular.extend(vm, response.data);
            var firstPage = Math.min(1, Math.abs(page - Math.floor(vm.pageSize / 2)));
            var lastPage = Math.min(firstPage + vm.pageSize - 1, vm.totalPages);

            vm.currentPages = (function () {
                var results = [];
                for (var i = firstPage; i <= lastPage; i++) {
                    results.push(i);
                }
                return results;
            }).apply(this);
        }

        function onError (error) {
            $mdToast.show(
                $mdToast.simple()
                    .content("ERROR! " + error.data.message)
                    .position("top right")
                    .hideDelay(3000)
            );
        }

    };
}


angular
    .module("app")
    .config(Configure);

function Configure ($stateProvider, $urlRouterProvider) {

    $stateProvider.state("login", {
        url: "/login",
        templateUrl: "views/login.html",
        controller: "LoginController as vm"
    });

    $stateProvider.state("home", {
        url: "/home",
        templateUrl: "views/home.html",
        controller: "HomeController as vm"
    });

    $stateProvider.state("search", {
        url: "/search",
        templateUrl: "views/search.html",
        controller: "SearchController as vm"
    });

    $urlRouterProvider.otherwise("/login");
}


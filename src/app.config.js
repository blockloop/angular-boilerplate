angular
    .module("app")
    .config(setupHttpInterceptor);

function setupHttpInterceptor($httpProvider, $provide) {
    $provide.factory("AuthHttpInterceptor", AuthHttpInterceptor);
    $httpProvider.interceptors.push("AuthHttpInterceptor");
}

function AuthHttpInterceptor($q, $window) {
    return {
        responseError: function (rejection) {
            if (rejection.status === 403) {
                $window.location.href = "/#/login";
            }
            $q.reject(rejection);
        }
    };
}


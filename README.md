## Setup

```
npm install
npm i -g gulp
gulp build server
```

Visit http://localhost:4000/ to see the application. Login functionality does not work because there is nothing listening at `/capi/`.

For transposing assets we use  gulp which needs to be installed globally. Once installed the available tasks can be found in the Gulpfile.js file. The tasks you will use most are `build`, `watch`, `prodBuild`, and `test`. To execute these you can simply run `gulp {task}` in command line (e.g. `gulp build` or `gulp build watch`). To read more visit [gulpjs.com][Gulp].

## Differences from John Papa

You'll notice that most of this boilerplate falls inline with [john papas angular-styleguide][angular-styleguide]. There are some minor differences.

 1. IIFE is managed in Gulp. Your JavaScript files should not contain IIFE wrappers.
 1. We use [Angular UI Router][]
 1. All routes are defined in one global place, not in separate files. See `/src/app.routes.js`.


 [Gulp]: http://gulpjs.com
 [angular-styleguide]: https://github.com/johnpapa/angularjs-styleguide
 [Angular UI Router]: https://github.com/angular-ui/ui-router

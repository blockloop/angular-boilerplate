(function () {
    var glob = require('glob').sync;

    module.exports = exports = {
        prod: {
            root: "public",
            js: {
                src: ["/app.min.js"],
                tpl: '<script src="%s"></script>'
            },
            css: {
                src: ["/app.min.css"],
                tpl: '<link rel="stylesheet" href="%s"/>'
            }
        },
        dev: {
            root: "dev_public",
            js: {
                src: getDevJsFiles(),
                tpl: '<script src="%s"></script>'
            },
            css: {
                src: getDevCssFiles(),
                tpl: '<link rel="stylesheet" href="%s"/>'
            }
        }
    }


    /*
     * INTERNAL FUNCTIONS
     */

    function getDevJsFiles() {
        var preReqs = [
            "js/lib.js",
            "js/app.js",
            "js/app.config.js",
            "js/app.routes.js"
        ];

        var more = glob('js/**/*.js', {
            cwd: "dev_public"
        }).filter(filter);

        return preReqs.concat(more);

        function filter(item) {
            return preReqs.indexOf(item) === -1
        }
    }

    function getDevCssFiles() {
        var preReqs = ["css/lib.css"];

        var more = glob('css/**/*.css', {
            cwd: "dev_public"
        }).filter(filter);

        return preReqs.concat(more);

        function filter(item){
            return preReqs.indexOf(item) === -1
        }
    }

}).call(this);

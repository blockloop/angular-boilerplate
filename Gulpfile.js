/* globals require, __dirname */
var gulp = require("gulp");
var gutil = require("gulp-util");
var concat = require("gulp-concat");
var minifyCss = require("gulp-minify-css");
var less = require("gulp-less");
var jade = require("gulp-jade");
var path = require("path");
var liveReload = require("gulp-livereload");
var cache = require("gulp-cached");
var jsHint = require("gulp-jshint");
var copy = require("gulp-copy");
var flatten = require('gulp-flatten');
var ngAnnotate = require('gulp-ng-annotate');
var addSrc = require('gulp-add-src');
var uglify = require('gulp-uglify');
var htmlReplace = require('gulp-html-replace');
var del = require('del');
var stylish = require('jshint-stylish');
var wrap = require('gulp-wrap');
var karma = require('karma').server;

/*
 * VARIABLES
 */
var EXPRESS_PORT = 4000;
var STATIC_PATH = path.join(__dirname, "dev_public/");
var assetsConfigs = require("./assets_config");

function handleError(err) {
    gutil.log(err);
    gutil.beep();
    this.emit("end");
}

function log(what, color) {
    var text = JSON.stringify(what);
    if (color != null) {
        text = gutil.colors[color](text);
    }
    return gutil.log(text);
}

function transposeJade(env, outputDir, pretty) {
    if (pretty == null) {
        pretty = true;
    }
    return gulp.src(paths.jade)
        .pipe(cache("jade"))
        .pipe(flatten({newPath: "views/"}))
        .pipe(addSrc(paths.indexJade))
        .pipe(jade({pretty: pretty})).on("error", handleError)
        .pipe(htmlReplace(assetsConfigs[env]))
        .pipe(gulp.dest(outputDir));
}

var paths = {
    js: [
        "src/app.js",
        "src/app.*.js",
        "src/**/*.js"
    ],
    jsLibs: [
        "lib/angular/angular.min.js",
        "lib/**/*.min.js"
    ],
    less: [
        "src/app.less",
        "src/**/*.less"
    ],
    jade: [
        "src/**/*.jade",
        "!src/index.jade"
    ],
    indexJade: ["src/index.jade"],
    cssLib: [
        "lib/**/*.min.css",
        "lib/angular-material/themes/*.css"
    ],
    fonts: ["lib/fonts/**/*"]
};


/*
 * TASKS
 */

gulp.task("js", function () {
    return gulp.src(paths.js)
        .pipe(cache("js"))
        .pipe(jsHint())
        .pipe(jsHint.reporter(stylish))
        .pipe(ngAnnotate())
        .pipe(wrap('(function(){ <%= contents %> })();'))
        .pipe(flatten())
        .pipe(gulp.dest("dev_public/js"));
});

gulp.task("fonts", function () {
    return gulp.src(paths.fonts)
        .pipe(copy("dev_public/fonts/", { prefix: 100 }))
        .pipe(gulp.dest('dev_public/fonts'));
});

gulp.task("cssLib", function () {
    return gulp.src(paths.cssLib)
        .pipe(concat("lib.css")).on("error", handleError)
        .pipe(gulp.dest("dev_public/css/"));
});

gulp.task("jsLib", function () {
    return gulp.src(paths.jsLibs)
        .pipe(concat("lib.js")).on("error", handleError)
        .pipe(gulp.dest("dev_public/js"));
});

gulp.task("jade", function () {
    return transposeJade("dev", "dev_public/");
});

gulp.task("less", function () {
    return gulp.src(paths.less)
        .pipe(less()).on("error", handleError)
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(flatten())
        .pipe(gulp.dest("dev_public/css/"));
});

gulp.task("clean", function () {
    del.sync(["dev_public/"], {force: true});
});

gulp.task("server", ["build", "watch"], function () {
    var express = require("express");
    var app = express();
    app.use(require("connect-livereload")());
    app.use(express["static"](STATIC_PATH));
    app.listen(EXPRESS_PORT);
    log("Express listening at http://localhost:" + EXPRESS_PORT, "cyan");
    return gulp.watch(STATIC_PATH + "**/*").on("change", liveReload.changed);
});

gulp.task("watch", function () {
    liveReload.listen();
    gulp.watch(paths.js, ["js", "test"]);
    gulp.watch(paths.jsLibs, ["jsLib"]);
    gulp.watch(paths.cssLib, ["cssLib"]);
    gulp.watch(paths.less, ["less"]);
    gulp.watch(paths.jade, ["jade"]);
    gulp.watch(paths.fonts, ["fonts"]);
});

gulp.task("test", function (done) {
    karma.start({
        configFile: __dirname + '/test/karma.conf.js'
        //, singleRun: false
    }, done);
});

gulp.task("build", [ "jsLib", "cssLib", "js", "less", "jade", "fonts" ]);

gulp.task("dev", ["build", "test", "watch", "server"]);


/*
 * PRODUCTION TASKS
 */

gulp.task("prodJs", ["jsLib", "js"], function () {
    return gulp.src([
        "dev_public/js/lib.js",
        "dev_public/js/app.js",
        "dev_public/js/app.*.js",
        "dev_public/js/**/*.js"])
        .pipe(concat("app.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest("public"));
});

gulp.task("prodCss", ["cssLib", "less"], function () {
    return gulp.src([
        "dev_public/css/lib.css",
        "dev_public/css/**/*.css"])
        .pipe(minifyCss({ keepSpecialComments: 0 }))
        .pipe(concat("app.min.css"))
        .pipe(gulp.dest("public"));
});

gulp.task("prodHtml", function () {
    return transposeJade("prod", "public");
});

gulp.task("prodFonts", function () {
    return gulp.src(paths.fonts)
        .pipe(copy("public/fonts/", { prefix: 100 }));
});

gulp.task("cleanProd", function () {
    del.sync(["public/**/*"], {force: true});
});

gulp.task("prodBuild", [
    "cleanProd",
    "prodJs",
    "prodCss",
    "prodHtml",
    "prodFonts"
]);


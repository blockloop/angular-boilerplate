require('coffee-script');

var tmpFile = function() {
	return '/tmp/' + (new Date()).getTime();
}

exports.config = {
	allScriptsTimeout: 11000,
	seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
	seleniumArgs: ['-browserTimeout=60'],
	chromeDriver: '../node_modules/chromedriver/bin/chromedriver',
		
	specs: [
		'e2e/app/**/*.test.coffee'
	],

	capabilities: {
		'browserName': 'phantomjs',
		'phantomjs.cli.args':['--local-storage-path='+tmpFile()]
	},

	stackTrace: false,

	baseUrl: 'http://localhost:8000',

	framework: 'jasmine',

	// DOES NOT WORK YET
	// SEE: https://github.com/angular/protractor/issues/38
	// plugins: [
	//	'protractor-coffee-preprocessor'
	// ],
	//

	onPrepare: function() {
		require('jasmine-spec-reporter');
		var reporter = new jasmine.SpecReporter({
			displayStackTrace: false,
			includeStackTrace: false
		});
		jasmine.getEnv().addReporter(reporter);
	},

	jasmineNodeOpts: {
		includeStackTrace: false,
		defaultTimeoutInterval: 30000,
		reporters: [],
	}
};

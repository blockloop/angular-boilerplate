/* globals describe, window, beforeEach, afterEach, it, expect */
(function () {
    describe('capi service', function () {
        beforeEach(module('app'));

        var capi, $q, $rootScope, $timeout, mockHttp, $http;

        beforeEach(function () {
            inject([
                "$rootScope", "$q", "$timeout", "capiService", "$httpBackend", "$http",
                function (rootScope, q, timeout, capiService, $httpBackend, http) {
                    $q = q;
                    $rootScope = rootScope;
                    $timeout = timeout;
                    capi = capiService;
                    mockHttp = $httpBackend;
                    $http = http;
                }
            ]);
        });

        afterEach(function() {
            mockHttp.verifyNoOutstandingRequest();
        });

        describe('login', function () {
            it('should set auth header upon success', function (done) {
                var mockToken = "kdsksdfksfdl";

                mockHttp.expectPOST('/capi/auth/login')
                    .respond(200, { token: mockToken });

                capi.login("user", "pw")
                    .then(complete, complete);

                mockHttp.flush();

                function complete() {
                    expect($http.defaults.headers.common.Authorization).toBe("Token " + mockToken);
                    done();
                }
            });
        });

        describe('logout', function () {
            it('should remove auth header upon success', function (done) {
                mockHttp.expectPOST('/capi/auth/logout')
                    .respond(200, {});

                capi.logout()
                    .then(complete, complete);

                mockHttp.flush();

                function complete() {
                    expect($http.defaults.headers.common.Authorization).toBeUndefined();
                    done();
                }
            });
        });
    });

}).call(this);
